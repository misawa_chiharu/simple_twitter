package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                  // user_id
            sql.append("    ?, ");                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");   // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getText());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void delete(Connection connection, int id) {
    	//「削除」ボタンが押されたらつぶやきを削除する操作。

    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("DELETE FROM messages WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void edit(Connection connection, int id) {
    	//「編集」ボタン押下でDBに登録されているつぶやきたちを呼び出す。
    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT * FROM messages WHERE id = ?");
            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, id);

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}

