package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId) {
    	final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            /*
             * idをnullで初期化
             * ServletからuserIdの値が渡ってきていたら
             * 整数型に型変換し、idに代入
             */
            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
            	id = Integer.parseInt(userId);
            	int num = id.intValue();
             }
            /*
             * messageDao.selectに引数としてInteger型のidを追加
             * idがnullだったら全件取得する
             * idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する
             */
            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int id) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection,id);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void edit(int id) {
    	Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().edit(connection,id);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}